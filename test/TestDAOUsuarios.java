/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import croketasRempalago_migueltabatasheila.dao.DAOFactory;
import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.util.List;
import org.junit.After;  
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alumno
 */
public class TestDAOUsuarios {

    public TestDAOUsuarios() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkobtenerUsuario() {

        int cont = 0;
        List<Usuario> usuarios = DAOFactory.getInstance().getDAOUsuarios().obtener();
        for (int i = 0; i < usuarios.size(); i++) {
            cont++;
        }
        assertEquals(cont, usuarios.size());

    }

    @Test
    public void checkRegistrarUsuario() {
        boolean cont=false;
        Usuario use= new Usuario("Mario","Perez","p@gmail.com","usuario.png","Feliz como una perdiz","peMario","mario123" );
        cont = DAOFactory.getInstance().getDAOUsuarios().registrarUsuario(use);
        assertEquals(cont,true);        
    }
    
    @Test
    public void checkActualizarUsuarioNombre(){
        boolean cont=false;
        Usuario u =null;
        u.setIdUsuario(29);
        cont = DAOFactory.getInstance().getDAOUsuarios().actualizarNombre(u,"H");
        assertEquals(cont,true); 
    }
    
    @Test
    public void checkActualizarUsuarioCorreo(){
        boolean cont=false;
        Usuario u =null;
        u.setIdUsuario(29);
        cont = DAOFactory.getInstance().getDAOUsuarios().actualizarCorreo(u, "h@gmail.com");
        assertEquals(cont,true);  
    }
    
    @Test
    public void checkActualizarUsuarioNick(){
        boolean cont=false;
        Usuario u =null;
        u.setIdUsuario(29);
        cont = DAOFactory.getInstance().getDAOUsuarios().actualizarCorreo(u, "Harold");
        assertEquals(cont,true);  
    }
    
    @Test
    public void checkActualizarUsuarioPass(){
        boolean cont=false;
        Usuario u =null;
        u.setIdUsuario(29);
        cont = DAOFactory.getInstance().getDAOUsuarios().actualizarPass(u, "harold123");
        assertEquals(cont,true);  
    }
    
    @Test
    public void checkActualizarUsuarioEstado(){
        boolean cont=false;
        Usuario u =null;
        u.setIdUsuario(29);
        cont = DAOFactory.getInstance().getDAOUsuarios().actualizarEstado(u, "Feliz y triste");
        assertEquals(cont,true);  
    }
    
}
