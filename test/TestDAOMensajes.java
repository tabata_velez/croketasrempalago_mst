/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import croketasRempalago_migueltabatasheila.dao.DAOFactory;
import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Mensaje;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.util.List;
import org.junit.After;  
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alumno
 */
public class TestDAOMensajes {

    public TestDAOMensajes() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkIntroducirMensaje() {
        Usuario uE = new Usuario();
        uE.setIdUsuario(29);
        Usuario uR = new Usuario();
        uR.setIdUsuario(30);
        
        Mensaje m = new Mensaje(uR,uE,"Hola Maria loca","19-02-2019","enviado");
        boolean cont = DAOFactory.getInstance().getDAOMensajes().insertarMensaje(m);
        assertEquals(cont, true);

    }

    @Test
    public void checkEliminarMensaje() {
        Usuario uE = new Usuario();
        uE.setIdUsuario(29);
        Usuario uR = new Usuario();
        uR.setIdUsuario(30);
        Mensaje m = new Mensaje();
        m.setIdMensaje(3);
        
        boolean cont = DAOFactory.getInstance().getDAOMensajes().eliminarMensaje(m);
        assertEquals(cont, true);
    }
    
    @Test
    public void checkintegrarGrupo(){
        boolean cont=false;
        Usuario uE = new Usuario();
        uE.setIdUsuario(29);
        Usuario uR = new Usuario();
        uR.setIdUsuario(30);
        cont= DAOFactory.getInstance().getDAOMensajes().verSiHayMensaje(uE, uR);
        
        
        assertEquals(cont,true);
    }
    
    public void checkobtenerListaDeMensajes() {
        int cont = 0;
        Usuario uE = new Usuario();
        uE.setIdUsuario(29);
        Usuario uR = new Usuario();
        uR.setIdUsuario(30);
        List<Mensaje> mensajeLista = DAOFactory.getInstance().getDAOMensajes().obtenerMensaje(uE, uR);
        for (int i = 0; i < mensajeLista.size(); i++) {
            cont++;
        }
        assertEquals(cont, mensajeLista.size());

    }
    
    
}
