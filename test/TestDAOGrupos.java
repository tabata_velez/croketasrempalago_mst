/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import croketasRempalago_migueltabatasheila.dao.DAOFactory;
import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.util.List;
import org.junit.After;  
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alumno
 */
public class TestDAOGrupos {

    public TestDAOGrupos() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkIntegrantesGrupo() {
        Grupo grupo = new Grupo();
        grupo.setIdGrupo(1);
        int cont = 0;
        List<Usuario> usuariosIntegrantes = DAOFactory.getInstance().getDAOGrupos().integrantesGrupo(grupo);
        for (int i = 0; i < usuariosIntegrantes.size(); i++) {
            cont++;
        }
        assertEquals(cont, usuariosIntegrantes.size());

    }

    @Test
    public void checkObtenerMisGrupos() {
        Usuario usuario = new Usuario();
        usuario.setIdUsuario(27);
        int cont=0;
        List<Grupo> usuariosIntegrantesGrupo = DAOFactory.getInstance().getDAOGrupos().obtenerGrupos(usuario);
        for (int i = 0; i < usuariosIntegrantesGrupo.size(); i++) {
            cont++;
            
        }
        assertEquals(cont, usuariosIntegrantesGrupo.size());        
    }
    
    @Test
    public void checkintegrarGrupo(){
        Grupo grupo = new Grupo();
        
        DAOFactory.getInstance().getDAOGrupos().integrantesGrupo(grupo);
        
        assertEquals(grupo, grupo);
    }
    
    public void checkobtenerUsuario() {
        int cont = 0;
        List<Usuario> usuarios = DAOFactory.getInstance().getDAOUsuarios().buscarUsuarios("t");
        for (int i = 0; i < usuarios.size(); i++) {
            cont++;
        }
        assertEquals(cont, usuarios.size());

    }
    
    
}
