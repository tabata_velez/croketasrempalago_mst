/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.modelo;

/**
 *
 * @author Tabata
 */
public class Mensaje {
    private int idMensaje;
    private Usuario idEmisor;
    private Usuario idReceptor;
    private String contenido;
    private String date;
    private String estado;
    
    
    //constructor

    public Mensaje() {
    }

    public Mensaje(Usuario idEmisor, Usuario idReceptor, String contenido, String date, String estado) {
        this.idEmisor = idEmisor;
        this.idReceptor = idReceptor;
        this.contenido = contenido;
        this.date = date;
        this.estado = estado;
    }
    
    //getters y setters 

    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Usuario getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(Usuario idEmisor) {
        this.idEmisor = idEmisor;
    }

    public Usuario getIdReceptor() {
        return idReceptor;
    }

    public void setIdReceptor(Usuario idReceptor) {
        this.idReceptor = idReceptor;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    //toString

    @Override
    public String toString() {
        return "Mensaje{" + "idMensaje=" + idMensaje + ", idEmisor=" + idEmisor + ", idReceptor=" + idReceptor + ", contenido=" + contenido + ", date=" + date + ", estado=" + estado + '}';
    }

    
    


}
