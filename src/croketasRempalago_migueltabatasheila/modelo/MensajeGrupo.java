/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.modelo;

import java.util.Date;

/**
 *
 * @author alumno
 */
public class MensajeGrupo {
    private int id;
    private Grupo grupo;
    private Usuario usuario;
    private String contenido;
    private String  fecha;
    private String estado;

    public MensajeGrupo(Grupo grupo, Usuario usuario, String contenido, String fecha,String estado) {
        this.grupo = grupo;
        this.usuario = usuario;
        this.contenido = contenido;
        this.fecha = fecha;
        this.estado = estado;
    }

    public MensajeGrupo() {
    }
    
    //getters y setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    //toString 

    @Override
    public String toString() {
        return "MensajeGrupo{" + "id=" + id + ", grupo=" + grupo + ", usuario=" + usuario + ", contenido=" + contenido + ", fecha=" + fecha + ", estado=" + estado + '}';
    }

   
    
}
