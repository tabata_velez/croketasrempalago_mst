/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tabata
 */
public class Grupo {
    private int idGrupo;
    private String nombre;
    private int tamanio;
    private String foto;
    private List<Usuario> listaGrupos;
    
    //constructor
    public Grupo(String nombre, int tamanio, String foto) {
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.foto = foto;
        this.listaGrupos = new ArrayList<Usuario>();
    }

    public Grupo() {
    }
    
    //getters y setters 

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public List<Usuario> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(List<Usuario> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }
    
    
    //toString

    @Override
    public String toString() {
        return "Grupo{" + "idGrupo=" + idGrupo + ", nombre=" + nombre + ", tamanio=" + tamanio + ", foto=" + foto + ", listaGrupos=" + listaGrupos + '}';
    }

    public void getListaGrupos(List<Grupo> grupol) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
