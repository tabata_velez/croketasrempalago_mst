/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.gui;

import croketasRempalago_migueltabatasheila.dao.DAOFactory;
import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import croketasRempalago_migueltabatasheila.modelo.WindowsCorreo;
import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author sheila
 */
public class ventanaAniadirParticipanteGrupo extends javax.swing.JFrame {

    private Usuario usuIni;
    private DefaultTableModel dtm;
    private JTable tabla;
    private Grupo grup;

    /**
     * Creates new form ventanaAniadirParticipanteGrupo
     */
    public ventanaAniadirParticipanteGrupo(Usuario usuario, Grupo grupo) {

        usuIni = usuario;
        grup = grupo;
        tabla = new JTable();
        dtm = new DefaultTableModel();
        dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }

        };
        tabla.setModel(dtm);
        initComponents();
        configuracionVentana();
        tabla();
    }

    private void configuracionVentana() {

        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(Color.BLACK);

        botonAddParticipante.setOpaque(false);
        botonAddParticipante.setContentAreaFilled(false);
        botonAddParticipante.setBorderPainted(false);
        botonAddParticipante.setForeground(Color.GREEN);

    }

    private void tabla() {
        String[] columnas = {"Todos los Usuarios"};
        JTableHeader header = tabla.getTableHeader();
        tabla.setGridColor(new java.awt.Color(51, 51, 51));
        tabla.setSelectionBackground(new java.awt.Color(255, 255, 204));
        tabla.setSelectionForeground(new java.awt.Color(0, 0, 0));
        tabla.setBackground(new java.awt.Color(204, 255, 204));
        header.setBackground(Color.GREEN);
        header.setForeground(Color.black);

        List<Usuario> listaTablaTodos = new ArrayList<Usuario>();
        listaTablaTodos = DAOFactory.getInstance().getDAOUsuarios().obtener();

        dtm.setColumnIdentifiers(columnas);
        for (int f = 0; f < listaTablaTodos.size(); f++) {

            String estadoUsuarioDos = DAOFactory.getInstance().getDAOAmigos().misAmigosVerEstado(listaTablaTodos.get(f), usuIni);
            if (!"bloqueado".equals(estadoUsuarioDos)) {
                Usuario user1 = listaTablaTodos.get(f);

                Object[] peliArray = new Object[3];
                peliArray[0] = user1.getNombre();

                dtm.addRow(peliArray);
            }
        }
        JScrollPane spTableBuscar = new JScrollPane(tabla);

        jLabel1.add(spTableBuscar);
        spTableBuscar.setBounds(30, 20, 117, 200);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botonAddParticipante = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        botonAddParticipante.setText("Añadir ");
        botonAddParticipante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAddParticipanteActionPerformed(evt);
            }
        });
        jPanel1.add(botonAddParticipante, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 117, -1));
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, 240));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAddParticipanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAddParticipanteActionPerformed
        Usuario usuarioPerfilSeleccionado = null;
        if (tabla.getSelectedRow() != -1) {
            String nombreTablaColumna = tabla.getValueAt(tabla.getSelectedRow(), 0).toString();

            List<Usuario> listaBuscar = new ArrayList<Usuario>();
            listaBuscar = DAOFactory.getInstance().getDAOUsuarios().obtener();

            for (int i = 0; i < listaBuscar.size(); i++) {

                if (nombreTablaColumna.equals(listaBuscar.get(i).getNombre())) {
                    usuarioPerfilSeleccionado = listaBuscar.get(i);
                }
            }
            
            DAOFactory.getInstance().getDAOGrupos().aniadirParticipante(usuarioPerfilSeleccionado, grup);
            WindowsCorreo winCorreo = new WindowsCorreo(usuarioPerfilSeleccionado, "tiene una solicitud de Grupo pendiente en el proyecto croketasRelampago, \n pase un buen dia.");
            winCorreo.SendMail();
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Selecciona un usuario.", "AVISO", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_botonAddParticipanteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAddParticipante;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
