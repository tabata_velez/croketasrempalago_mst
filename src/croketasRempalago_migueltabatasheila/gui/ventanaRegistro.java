/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.gui;

import croketasRempalago_migueltabatasheila.dao.DAOFactory;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.awt.Color;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author sheila
 */
public class ventanaRegistro extends javax.swing.JFrame {

    /**
     * Creates new form ventanaRegistro
     */
    public ventanaRegistro() {
        initComponents();
        configuracionVentana();
        ocultarBordeBotonSeleccionImagen();
        colorLabels();
    }

    private void configuracionVentana(){
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    private void ocultarBordeBotonSeleccionImagen() {
        seleccionImg.setOpaque(false);
        seleccionImg.setContentAreaFilled(false);
        seleccionImg.setBorderPainted(false);
        seleccionImg.setForeground(Color.WHITE);

        this.fondoRegistro.setIcon(new ImageIcon("img/fondoInicioSesion.jpg"));
    }

    private void colorLabels() {
        etiquetaApellido.setForeground(Color.WHITE);
        etiquetaNombre.setForeground(Color.WHITE);
        etiquetaNick.setForeground(Color.WHITE);
        etiquetaEstado.setForeground(Color.WHITE);
        etiquetaContrasenia.setForeground(Color.WHITE);
        etiquetaCorreo.setForeground(Color.WHITE);

        botonRegistrar.setOpaque(false);
        botonRegistrar.setContentAreaFilled(false);
        botonRegistrar.setBorderPainted(false);
        botonRegistrar.setForeground(Color.white);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        seleccionImg = new javax.swing.JButton();
        iconoUsuario = new javax.swing.JLabel();
        etiquetaNombre = new javax.swing.JLabel();
        registrarNombre = new javax.swing.JTextField();
        etiquetaApellido = new javax.swing.JLabel();
        registroApellido = new javax.swing.JTextField();
        etiquetaCorreo = new javax.swing.JLabel();
        registroCorreo = new javax.swing.JTextField();
        etiquetaContrasenia = new javax.swing.JLabel();
        registroContraseña = new javax.swing.JPasswordField();
        etiquetaNick = new javax.swing.JLabel();
        etiquetaEstado = new javax.swing.JLabel();
        registroNick = new javax.swing.JTextField();
        registroEstado = new javax.swing.JTextField();
        botonRegistrar = new javax.swing.JButton();
        fondoRegistro = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        seleccionImg.setForeground(new java.awt.Color(255, 255, 255));
        seleccionImg.setText("selecciona una imagen");
        seleccionImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seleccionImgActionPerformed(evt);
            }
        });
        jPanel1.add(seleccionImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 171, 41));
        jPanel1.add(iconoUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 23, 60, 50));

        etiquetaNombre.setText("Nombre");
        jPanel1.add(etiquetaNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 50, 20));

        registrarNombre.setText("Escriba su nombre");
        registrarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarNombreActionPerformed(evt);
            }
        });
        jPanel1.add(registrarNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 160, -1));

        etiquetaApellido.setText("Apellido");
        jPanel1.add(etiquetaApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 50, -1));

        registroApellido.setText("Escriba su apellido");
        jPanel1.add(registroApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 160, -1));

        etiquetaCorreo.setText("Correo");
        jPanel1.add(etiquetaCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 50, -1));

        registroCorreo.setText("ejemplo@gmail.com");
        jPanel1.add(registroCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 140, 160, -1));

        etiquetaContrasenia.setText("Contraseña");
        jPanel1.add(etiquetaContrasenia, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 80, -1));

        registroContraseña.setText("jPasswordField1");
        jPanel1.add(registroContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, 160, -1));

        etiquetaNick.setText("Nick");
        jPanel1.add(etiquetaNick, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, -1));

        etiquetaEstado.setText("Estado");
        jPanel1.add(etiquetaEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 50, -1));

        registroNick.setText("Nick");
        jPanel1.add(registroNick, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 160, -1));

        registroEstado.setText("Estado: feliz , triste etc...");
        jPanel1.add(registroEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 230, 160, -1));

        botonRegistrar.setForeground(new java.awt.Color(255, 255, 255));
        botonRegistrar.setText("Registrarse");
        botonRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRegistrarActionPerformed(evt);
            }
        });
        jPanel1.add(botonRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 160, -1));
        jPanel1.add(fondoRegistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, 0, 280, 310));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 258, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 297, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void seleccionImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seleccionImgActionPerformed
        // TODO add your handling code here:
        //Abrir cuadro para seleccionar img
        JFileChooser file = new JFileChooser();
        file.showOpenDialog(this);
        File archivo = file.getSelectedFile();
        //verificamos que sea una img
        if (archivo != null) {
            //obtenemos la ruta de la img
            String origen = archivo.getPath();
            //creamos el objeto con la imagen
            ImageIcon icon = new ImageIcon(origen);

            this.iconoUsuario.setIcon(icon);

        } else {
            JOptionPane.showMessageDialog(null, "Por favor seleccione una imagen");
        }

    }//GEN-LAST:event_seleccionImgActionPerformed

    private void registrarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarNombreActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_registrarNombreActionPerformed

    private void botonRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRegistrarActionPerformed

        // TODO add your handling code here:
        boolean correo = false;

        List<Usuario> listaUsuarioObtenerUsuario = new ArrayList<Usuario>();
        listaUsuarioObtenerUsuario = DAOFactory.getInstance().getDAOUsuarios().obtener();
        for (int m = 0; m < listaUsuarioObtenerUsuario.size(); m++) {
            if (listaUsuarioObtenerUsuario.get(m).getCorreo().equals(registroCorreo.getText())) {
                correo = true;
            }
        }

        if (correo == false) {

            
            char[] arrayC = registroContraseña.getPassword();
            String pass = new String(arrayC);

            String foto = iconoUsuario.getIcon().toString();


            foto = foto.split("img")[1];
            foto = foto.substring(1);
            boolean registro = false;
            Usuario usuario = new Usuario(registrarNombre.getText(), registroApellido.getText(), registroCorreo.getText(), foto, registroEstado.getText(), registroNick.getText(), pass);
            //ver la ruta de la img
            Usuario usu = null;
            registro = DAOFactory.getInstance().getDAOUsuarios().registrarUsuario(usuario);
            if (registro == true) {
                List<Usuario> listaUsuarioObtener = new ArrayList<Usuario>();
                listaUsuarioObtener = DAOFactory.getInstance().getDAOUsuarios().obtener();
                for (int i = 0; i < listaUsuarioObtener.size(); i++) {
                    if (listaUsuarioObtener.get(i).getCorreo().equals(registroCorreo.getText())) {
                        usu = listaUsuarioObtener.get(i);
                    }
                }
                DAOFactory.getInstance().getDAOUsuarios().actualizarPass(usu, pass);
                JOptionPane.showMessageDialog(null, "Usuario registrado");
                this.dispose();
            }
            
        } else {
            
            JOptionPane.showMessageDialog(null, "Correo ya registrado");
        }
    }//GEN-LAST:event_botonRegistrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonRegistrar;
    private javax.swing.JLabel etiquetaApellido;
    private javax.swing.JLabel etiquetaContrasenia;
    private javax.swing.JLabel etiquetaCorreo;
    private javax.swing.JLabel etiquetaEstado;
    private javax.swing.JLabel etiquetaNick;
    private javax.swing.JLabel etiquetaNombre;
    private javax.swing.JLabel fondoRegistro;
    private javax.swing.JLabel iconoUsuario;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField registrarNombre;
    private javax.swing.JTextField registroApellido;
    private javax.swing.JPasswordField registroContraseña;
    private javax.swing.JTextField registroCorreo;
    private javax.swing.JTextField registroEstado;
    private javax.swing.JTextField registroNick;
    private javax.swing.JButton seleccionImg;
    // End of variables declaration//GEN-END:variables
}
