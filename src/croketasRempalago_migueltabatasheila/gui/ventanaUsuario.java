/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.gui;

import croketasRempalago_migueltabatasheila.dao.DAOFactory;
import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author alumno
 */
public class ventanaUsuario extends javax.swing.JFrame {
    
    private Usuario usr;
    private Usuario usr1;
    private DefaultTableModel dtm;
    private DefaultTableModel dtmAmigos;

    /**
     * Creates new form ventanaUsuario
     */
    public ventanaUsuario(Usuario user) {
        usr1 = user;
        usr = sacarUsuarioCorreo(usr, user.getCorreo());
        dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
            
        };
        
        dtmAmigos = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        
        initComponents();
        configuracionVentana();
        addTabla();
        addTablaAmigos(usr);
        
    }
    
    private void addTabla() {
        tablaGrupos.setModel(dtm);
        dtm.addColumn("Mis grupos");
        dtm.addColumn("Tamaño");
        List<Grupo> listaGrupos = new ArrayList<Grupo>();
        listaGrupos = DAOFactory.getInstance().getDAOGrupos().obtenerGrupos(usr);
        
        for (int k = 0; k < listaGrupos.size(); k++) {
            String estadoUsuarioDos = DAOFactory.getInstance().getDAOGrupos().misGruposVerEstado(usr, listaGrupos.get(k));
            if (!"pendiente".equals(estadoUsuarioDos)) {
                Object[] fila = new Object[2];
                Grupo gr = listaGrupos.get(k);
                fila[0] = gr.getNombre();
                fila[1] = gr.getTamanio();
                dtm.addRow(fila);
            }
        }
        tablaGrupos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
    }
    
    private void addTablaAmigos(Usuario usrAmigos) {
        tablaAmigos.setModel(dtmAmigos);
        dtmAmigos.addColumn("Mis amigos");
        
        List<Usuario> listaTabla = new ArrayList<Usuario>();
        
        List<Usuario> listaComparacion = new ArrayList<Usuario>();
        listaComparacion = DAOFactory.getInstance().getDAOUsuarios().obtener();
        
        List<Integer> userLista = new ArrayList<Integer>();
        userLista = DAOFactory.getInstance().getDAOAmigos().obtenerMisAmigos(usrAmigos);
        
        for (int f = 0; f < userLista.size(); f++) {
            for (int m = 0; m < listaComparacion.size(); m++) {
                if (userLista.get(f) == listaComparacion.get(m).getIdUsuario()) {
                    listaTabla.add(listaComparacion.get(m));
                }
            }
        }
        
        Object[] fila = new Object[1];
        for (int i = 0; i < userLista.size(); i++) {
            Usuario us = listaTabla.get(i);
            fila[0] = us.getNombre();
            dtmAmigos.addRow(fila);
        }
        tablaAmigos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
    }
    
    private void configuracionVentana() {
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        jPanel1.setBackground(Color.BLACK);
        
        JTableHeader TheaderUsuario = tablaAmigos.getTableHeader();
        TheaderUsuario.setBackground(Color.GREEN); // cambiar el color de fondo
        TheaderUsuario.setForeground(Color.black); // cambiar el primer plano

        JTableHeader TheaderGrupo = tablaGrupos.getTableHeader();
        TheaderGrupo.setBackground(Color.red); // cambiar el color de fondo
        TheaderGrupo.setForeground(Color.WHITE); // cambiar el primer plano
        
        MenuUsuario.setBackground(Color.WHITE);
        menuUsuario1.setBackground(Color.WHITE);
        menuUsuario1.setForeground(Color.black);
        menuUsuario2.setBackground(Color.WHITE);
        menuUsuario2.setForeground(Color.black);
        
        botonMandarMsn.setOpaque(false);
        botonMandarMsn.setContentAreaFilled(false);
        botonMandarMsn.setBorderPainted(false);
        botonMandarMsn.setForeground(Color.GREEN);
        
        botonVerPerfil.setOpaque(false);
        botonVerPerfil.setContentAreaFilled(false);
        botonVerPerfil.setBorderPainted(false);
        botonVerPerfil.setForeground(Color.GREEN);
        
        botonMandarMsnGrupo.setOpaque(false);
        botonMandarMsnGrupo.setContentAreaFilled(false);
        botonMandarMsnGrupo.setBorderPainted(false);
        botonMandarMsnGrupo.setForeground(Color.RED);
        
        botonVerPerfilGrupo.setOpaque(false);
        botonVerPerfilGrupo.setContentAreaFilled(false);
        botonVerPerfilGrupo.setBorderPainted(false);
        botonVerPerfilGrupo.setForeground(Color.RED);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botonMandarMsn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaGrupos = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaAmigos = new javax.swing.JTable();
        botonVerPerfil = new javax.swing.JButton();
        botonMandarMsnGrupo = new javax.swing.JButton();
        botonVerPerfilGrupo = new javax.swing.JButton();
        MenuUsuario = new javax.swing.JMenuBar();
        menuUsuario1 = new javax.swing.JMenu();
        buscarAmigos = new javax.swing.JMenuItem();
        misAmigos = new javax.swing.JMenuItem();
        solicitud = new javax.swing.JMenuItem();
        solicitudGruposmenu = new javax.swing.JMenuItem();
        botonBloqueados = new javax.swing.JMenuItem();
        jmenuCrearGrupo = new javax.swing.JMenuItem();
        menuUsuario2 = new javax.swing.JMenu();
        configuracionPerfil = new javax.swing.JMenuItem();
        cerrarSesion = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        botonMandarMsn.setText("Enviar mensaje");
        botonMandarMsn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMandarMsnActionPerformed(evt);
            }
        });
        jPanel1.add(botonMandarMsn, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

        tablaGrupos.setBackground(new java.awt.Color(255, 153, 153));
        tablaGrupos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tablaGrupos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "mis grupos", "Tamaño"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaGrupos.setColumnSelectionAllowed(true);
        tablaGrupos.setGridColor(new java.awt.Color(255, 0, 51));
        tablaGrupos.setSelectionBackground(new java.awt.Color(102, 255, 204));
        tablaGrupos.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tablaGrupos);
        tablaGrupos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(307, 13, 202, 168));

        tablaAmigos.setBackground(new java.awt.Color(204, 255, 204));
        tablaAmigos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaAmigos.setColumnSelectionAllowed(true);
        tablaAmigos.setGridColor(new java.awt.Color(0, 153, 0));
        tablaAmigos.setSelectionBackground(new java.awt.Color(255, 255, 204));
        tablaAmigos.setSelectionForeground(new java.awt.Color(0, 0, 0));
        tablaAmigos.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tablaAmigos);
        tablaAmigos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 13, 175, 168));

        botonVerPerfil.setText("Ir a perfil");
        botonVerPerfil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonVerPerfilActionPerformed(evt);
            }
        });
        jPanel1.add(botonVerPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, -1, -1));

        botonMandarMsnGrupo.setText("Enviar mensaje");
        botonMandarMsnGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMandarMsnGrupoActionPerformed(evt);
            }
        });
        jPanel1.add(botonMandarMsnGrupo, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 190, -1, -1));

        botonVerPerfilGrupo.setText("Ir a perfil");
        botonVerPerfilGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonVerPerfilGrupoActionPerformed(evt);
            }
        });
        jPanel1.add(botonVerPerfilGrupo, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 220, -1, -1));

        menuUsuario1.setText("Amigos");

        buscarAmigos.setText("Buscar amigos");
        buscarAmigos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarAmigosActionPerformed(evt);
            }
        });
        menuUsuario1.add(buscarAmigos);

        misAmigos.setText("Mis Mensajes");
        misAmigos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                misAmigosActionPerformed(evt);
            }
        });
        menuUsuario1.add(misAmigos);

        solicitud.setText("Solicitud");
        solicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                solicitudActionPerformed(evt);
            }
        });
        menuUsuario1.add(solicitud);

        solicitudGruposmenu.setText("Solicitud Grupos");
        solicitudGruposmenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                solicitudGruposmenuActionPerformed(evt);
            }
        });
        menuUsuario1.add(solicitudGruposmenu);

        botonBloqueados.setText("Bloqueados");
        botonBloqueados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBloqueadosActionPerformed(evt);
            }
        });
        menuUsuario1.add(botonBloqueados);

        jmenuCrearGrupo.setText("Crear Grupo");
        jmenuCrearGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmenuCrearGrupoActionPerformed(evt);
            }
        });
        menuUsuario1.add(jmenuCrearGrupo);

        MenuUsuario.add(menuUsuario1);

        menuUsuario2.setText("Perfil");

        configuracionPerfil.setText("Configuracion");
        configuracionPerfil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configuracionPerfilActionPerformed(evt);
            }
        });
        menuUsuario2.add(configuracionPerfil);

        cerrarSesion.setText("Finalizar Sesion");
        cerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarSesionActionPerformed(evt);
            }
        });
        menuUsuario2.add(cerrarSesion);

        MenuUsuario.add(menuUsuario2);

        setJMenuBar(MenuUsuario);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void misAmigosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_misAmigosActionPerformed
        // TODO add your handling code here:
        new ventanaMisMensajes(usr).setVisible(true);
    }//GEN-LAST:event_misAmigosActionPerformed
    

    private void configuracionPerfilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configuracionPerfilActionPerformed
        // TODO add your handling code here:
        new ventanaConfiguracionUsuario(usr1).setVisible(true);
    }//GEN-LAST:event_configuracionPerfilActionPerformed

    private void botonVerPerfilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonVerPerfilActionPerformed
        Usuario usuarioPerfilSeleccionado = null;
        
        if (tablaAmigos.getSelectedRow() != -1) {
            if (tablaAmigos.getSelectedRow() != -1) {
                String nombreTablaColumna = tablaAmigos.getValueAt(tablaAmigos.getSelectedRow(), 0).toString();
                usuarioPerfilSeleccionado = sacarUsuarioNombre(usr, nombreTablaColumna);
                new ventanaPerfil(usuarioPerfilSeleccionado, this).setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Selecciona un amigo.", "AVISO", JOptionPane.WARNING_MESSAGE);
            }
        } else if (tablaGrupos.getSelectedRow() != -1) {
            JOptionPane.showMessageDialog(null, "Selecciona un amigo.", "AVISO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_botonVerPerfilActionPerformed

    private void buscarAmigosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarAmigosActionPerformed
        // TODO add your handling code here:.
        new ventanaBusquedaAmigos().setVisible(true);
    }//GEN-LAST:event_buscarAmigosActionPerformed

    private void solicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_solicitudActionPerformed
        // TODO add your handling code here:
        new ventanaSolicitud(this).setVisible(true);

    }//GEN-LAST:event_solicitudActionPerformed

    private void cerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarSesionActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new ventanaInicio().setVisible(true);

    }//GEN-LAST:event_cerrarSesionActionPerformed

    private void botonMandarMsnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMandarMsnActionPerformed
        // TODO add your handling code here:
        Usuario usuarioPerfilSeleccionado = null;
        if (tablaAmigos.getSelectedRow() != -1) {
            if (tablaAmigos.getSelectedRow() != -1) {
                String nombreTablaColumna = tablaAmigos.getValueAt(tablaAmigos.getSelectedRow(), 0).toString();
                usuarioPerfilSeleccionado = sacarUsuarioNombre(usr, nombreTablaColumna);
                new ventanaMensajes(usuarioPerfilSeleccionado,"").setVisible(true);
            }
        } else if (tablaGrupos.getSelectedRow() != -1) {
            JOptionPane.showMessageDialog(null, "Selecciona un amigo.", "AVISO", JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Selecciona un amigo.", "AVISO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_botonMandarMsnActionPerformed

    private void botonBloqueadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBloqueadosActionPerformed
        new ventanaBloqueados().setVisible(true);
    }//GEN-LAST:event_botonBloqueadosActionPerformed

    private void botonMandarMsnGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMandarMsnGrupoActionPerformed
        Usuario usuarioPerfilSeleccionado = null;
        if (tablaGrupos.getSelectedRow() != -1) {
            if (tablaGrupos.getSelectedRow() != -1) {
                String nombreTablaColumna = tablaGrupos.getValueAt(tablaGrupos.getSelectedRow(), 0).toString();
                //LLAMARA A LA VENTANA MENSAJES GRUPO
                List<Grupo> listaG = new ArrayList<Grupo>();
                listaG = DAOFactory.getInstance().getDAOGrupos().obtenerGrupos(usr);
                Grupo grupo = null;
                for (int k = 0; k < listaG.size(); k++) {
                    if (nombreTablaColumna.equals(listaG.get(k).getNombre())) {
                        grupo = listaG.get(k);
                    }
                }
                
                new ventanaMensajesGrupo(grupo,"").setVisible(true);
                
            }
        } else if (tablaAmigos.getSelectedRow() != -1) {
            JOptionPane.showMessageDialog(null, "Selecciona un grupo.", "AVISO", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_botonMandarMsnGrupoActionPerformed

    private void botonVerPerfilGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonVerPerfilGrupoActionPerformed
        
        Grupo grupoSeleccionado = null;
        if (tablaGrupos.getSelectedRow() != -1) {
            if (tablaGrupos.getSelectedRow() != -1) {
                String nombreTablaColumna = tablaGrupos.getValueAt(tablaGrupos.getSelectedRow(), 0).toString();
                grupoSeleccionado = sacarGrupoNombre(usr, nombreTablaColumna);
                new ventanaMisGrupos(grupoSeleccionado, this).setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Selecciona un grupo.", "AVISO", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_botonVerPerfilGrupoActionPerformed

    private void solicitudGruposmenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_solicitudGruposmenuActionPerformed
        new ventanaSolicitudGrupos(this).setVisible(true);
    }//GEN-LAST:event_solicitudGruposmenuActionPerformed

    private void jmenuCrearGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmenuCrearGrupoActionPerformed
        
        new ventanaCrearGrupo(usr1,this).setVisible(true);
    }//GEN-LAST:event_jmenuCrearGrupoActionPerformed
    
    private Usuario sacarUsuarioCorreo(Usuario usuario, String correo) {
        List<Usuario> listaBuscar = new ArrayList<Usuario>();
        listaBuscar = DAOFactory.getInstance().getDAOUsuarios().obtener();
        
        for (int i = 0; i < listaBuscar.size(); i++) {
            if (correo.equals(listaBuscar.get(i).getCorreo())) {
                usuario = listaBuscar.get(i);
            }
        }
        
        return usuario;
    }
    
    private Grupo sacarGrupoNombre(Usuario usuario, String nombre) {
        Grupo grupo = null;
        List<Grupo> listaBuscarGrupos = new ArrayList<Grupo>();
        listaBuscarGrupos = DAOFactory.getInstance().getDAOGrupos().obtenerGrupos(usuario);
        
        for (int i = 0; i < listaBuscarGrupos.size(); i++) {
            if (nombre.equals(listaBuscarGrupos.get(i).getNombre())) {
                grupo = listaBuscarGrupos.get(i);
            }
        }
        return grupo;
    }
    
    private Usuario sacarUsuarioNombre(Usuario usuario, String nombre) {
        List<Usuario> listaBuscar = new ArrayList<Usuario>();
        listaBuscar = DAOFactory.getInstance().getDAOUsuarios().obtener();
        
        for (int i = 0; i < listaBuscar.size(); i++) {
            if (nombre.equals(listaBuscar.get(i).getNombre())) {
                usuario = listaBuscar.get(i);
            }
        }
        
        return usuario;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar MenuUsuario;
    private javax.swing.JMenuItem botonBloqueados;
    private javax.swing.JButton botonMandarMsn;
    private javax.swing.JButton botonMandarMsnGrupo;
    private javax.swing.JButton botonVerPerfil;
    private javax.swing.JButton botonVerPerfilGrupo;
    private javax.swing.JMenuItem buscarAmigos;
    private javax.swing.JMenuItem cerrarSesion;
    private javax.swing.JMenuItem configuracionPerfil;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuItem jmenuCrearGrupo;
    private javax.swing.JMenu menuUsuario1;
    private javax.swing.JMenu menuUsuario2;
    private javax.swing.JMenuItem misAmigos;
    private javax.swing.JMenuItem solicitud;
    private javax.swing.JMenuItem solicitudGruposmenu;
    private javax.swing.JTable tablaAmigos;
    private javax.swing.JTable tablaGrupos;
    // End of variables declaration//GEN-END:variables
}
