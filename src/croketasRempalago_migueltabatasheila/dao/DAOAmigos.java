/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.util.List;

/**
 *
 * @author Alumno
 */
public interface DAOAmigos {

    public boolean boton(Usuario userPri, Usuario userSec);

    public boolean bloquearCambiarTipo(Usuario usu1, Usuario usu2);

    public boolean eliminarDeAmigos(Usuario usuPri, Usuario usuSec);

    public String misAmigosVerEstado(Usuario usuPri, Usuario usuSec);

    public boolean averiguarSiestaAmigos(Usuario usuPri, Usuario usuSec);

    public void insertarBloqueado(Usuario usuPri, Usuario usuSec);

    public boolean desbloquearAmigo(Usuario usuIni, Usuario usuBloqueado);

    public void enviarSolicitudAmistad(Usuario usu1, Usuario usu2);

    public List<Usuario> solicitudesPendientes(Usuario usuIni);

    public void aceptarAmigos(Usuario usuIni, Usuario usuPerfil);

    public void insertarAmigoNuevo(Usuario usuPerfil, Usuario usuIni);

    public List<Integer> obtenerMisAmigos(Usuario usuario);

    public void actualizarEstdoAmigos(Usuario usuIni, Usuario usuPerfil);

}
