/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import croketasRempalago_migueltabatasheila.modelo.Mensaje;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tabata
 */
public class DAOMensajeSQL implements DAOMensaje{
   @Override
    public boolean insertarMensaje(Mensaje mensaje) {
       boolean registrar = false;

        Statement stm = null;
        Connection con = null;
        
        String sql = "INSERT INTO mensajes VALUES ("+null+","+mensaje.getIdEmisor().getIdUsuario()+","
                + ""+mensaje.getIdReceptor().getIdUsuario()+",'"+mensaje.getContenido()+"','"+mensaje.getDate()+"','enviado')";
        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            registrar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método insertar mensajes");
            e.printStackTrace();
        }
        return registrar;
    }

    @Override
    public boolean eliminarMensaje(Mensaje mensaje) {
        Connection connect = null;
        Statement stm = null;

        boolean borrar = false;


        String sql = "DELETE FROM mensajes  WHERE  id_mensaje =  " + mensaje.getIdMensaje();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            borrar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();

        }

        return borrar; 
    }

    @Override
    public List<Mensaje> obtenerMensaje(Usuario usuInicio, Usuario usuPerfil) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        
        String sql = "SELECT * FROM  mensajes where id_receptor = "+usuInicio.getIdUsuario()+" and id_emisor ="+usuPerfil.getIdUsuario()+" OR id_receptor ="+usuPerfil.getIdUsuario()+" and id_emisor ="+usuInicio.getIdUsuario()+" ORDER by fecha asc";
        List<Mensaje> listaMensaje = new ArrayList<Mensaje>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                Mensaje c = new Mensaje();
                
                Usuario r = new Usuario();
                r.setIdUsuario(rs.getInt(3));
                
                Usuario e = new Usuario();
                e.setIdUsuario(rs.getInt(2));
                
                c.setIdMensaje(rs.getInt(1));
                c.setIdEmisor(e);
                c.setIdReceptor(r);
                c.setContenido(rs.getString(4));
                c.setDate(rs.getString(5));
                c.setEstado(rs.getString(6));


                listaMensaje.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaMensaje;
    } 

    @Override
    public boolean verSiHayMensaje(Usuario usuIni, Usuario usuPerfil) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        boolean hay=false;
        
        String sql = "SELECT * FROM mensajes WHERE id_emisor = "+usuIni.getIdUsuario()+" AND id_receptor ="+usuPerfil.getIdUsuario()+" OR id_emisor = "+usuPerfil.getIdUsuario()+" AND id_receptor ="+usuIni.getIdUsuario();
 
        

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                hay = true;
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return hay;   
    }
}
