
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import croketasRempalago_migueltabatasheila.modelo.Mensaje;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.util.List;

/**
 *
 * @author Tabata
 */
public interface DAOMensaje {
    public boolean insertarMensaje(Mensaje mensaje);
    public boolean eliminarMensaje(Mensaje mensaje);
    public List<Mensaje> obtenerMensaje(Usuario usuIni, Usuario usuPerfil);
    public boolean verSiHayMensaje(Usuario usuIni, Usuario usuPerfil);
}
