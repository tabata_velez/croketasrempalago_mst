/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import croketasRempalago_migueltabatasheila.modelo.Usuario;

/**
 *
 * @author Tabata
 */
public class DAOUsuarioSQL implements DAOUsuario {

    @Override
    public boolean registrarUsuario(Usuario usuario) {
        boolean registrar = false;

        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO usuarios values (" + null + ",'"
                + usuario.getNombre() + "','" + usuario.getApellido() + "','" + usuario.getCorreo() + "','"
                + usuario.getEstado() + "','" + usuario.getNick() + "','" + usuario.getContrasenia()
                + "','" + usuario.getFoto() + "')";

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            registrar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
        return registrar;
    }

    @Override
    public boolean borrarUsuario(Usuario usuario) {
        Connection connect = null;
        Statement stm = null;

        boolean borrar = false;

        //DELETE FROM `usuarios` WHERE `id_usuario` = 2 AND cargo ="administrador" 
        String sql = "DELETE FROM usuarios WHERE id_usuario= " + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            borrar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();

        }

        return borrar;

    }

    @Override
    public List<Usuario> obtener() {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM usuarios";

        List<Usuario> listaUsuarios = new ArrayList<Usuario>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                Usuario c = new Usuario();
                c.setIdUsuario(rs.getInt(1));
                c.setNombre(rs.getString(2));
                c.setApellido(rs.getString(3));
                c.setCorreo(rs.getString(4));
                c.setEstado(rs.getString(5));
                c.setNick(rs.getString(6));
                c.setContrasenia(rs.getString(7));
                c.setFoto(rs.getString(8));

                listaUsuarios.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaUsuarios;
    }

    @Override
    public boolean actualizarNombre(Usuario usuario, String nombreACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET nombre='" + nombreACambiar + "' WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarApellido(Usuario usuario, String apellidoACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET apellido='" + apellidoACambiar + "' WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarCorreo(Usuario usuario, String correoACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET correo='" + correoACambiar + "' WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarEstado(Usuario usuario, String estadoACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET estado='" + estadoACambiar + "' WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarNick(Usuario usuario, String nickACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET nick='" + nickACambiar + "' WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarPass(Usuario usuario, String passACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET pass= MD5('" + passACambiar + "') WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarFoto(Usuario usuario, String fotoACambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE usuarios SET foto='" + fotoACambiar + "' WHERE id_user=" + usuario.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public String passEncriptada(String contrasenya) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT MD5('" + contrasenya + "')";
        String pass = null;

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                pass = rs.getString(1);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener mis amigos");
            e.printStackTrace();
        }

        return pass;
    }

    @Override
    public List<Usuario> buscarUsuarios(String buscarUsuario) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM `usuarios` WHERE nombre LIKE '%" + buscarUsuario + "%'";

        List<Usuario> listaUsu = new ArrayList<Usuario>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                Usuario c = new Usuario();
                c.setIdUsuario(rs.getInt(1));
                c.setNombre(rs.getString(2));
                c.setApellido(rs.getString(3));
                c.setCorreo(rs.getString(4));
                c.setEstado(rs.getString(5));
                c.setNick(rs.getString(6));
                c.setContrasenia(rs.getString(7));
                c.setFoto(rs.getString(8));

                listaUsu.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaUsu;

    }
}
