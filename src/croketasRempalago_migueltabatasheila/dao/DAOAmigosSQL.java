/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alumno
 */
public class DAOAmigosSQL implements DAOAmigos {

    @Override
    public List<Integer> obtenerMisAmigos(Usuario usuario) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        String tipo = "amigos";
        String sql = "SELECT id_userSec from amigos where (id_userPri = " + usuario.getIdUsuario() + ") AND (tipo = '" + tipo + "')";

        List<Integer> listaUsuarios = new ArrayList<Integer>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                int id_Sec;
                id_Sec = rs.getInt(1);

                listaUsuarios.add(id_Sec);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener mis amigos");
            e.printStackTrace();
        }

        return listaUsuarios;
    }

    @Override
    public boolean boton(Usuario userPri, Usuario userSec) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        boolean consulta = false;
        String sql = "SELECT * FROM usuarios"
                + " WHERE id_user not in "
                + "(SELECT a.id_userSec "
                + "FROM amigos a JOIN usuarios u ON u.id_user = a.id_userSec "
                + "WHERE a.id_userPri =" + userPri.getIdUsuario() + " AND u.nombre = '" + userSec.getNombre() + "') "
                + "AND nombre='" + userSec.getNombre() + "'";

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                consulta = true;
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método boton de amigos");
            e.printStackTrace();
        }

        return consulta;
    }

    @Override
    public boolean bloquearCambiarTipo(Usuario usu1, Usuario usu2) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE `amigos` SET `tipo` = 'bloqueado' WHERE `amigos`.`id_userPri` = " + usu1.getIdUsuario() + " AND `amigos`.`id_userSec` = " + usu2.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;

    }

    @Override
    public boolean eliminarDeAmigos(Usuario usuPri, Usuario usuSec) {
        Connection connect = null;
        Statement stm = null;

        boolean borrar = false;

        //DELETE FROM `usuarios` WHERE `id_usuario` = 2 AND cargo ="administrador" 
        String sql = "DELETE FROM amigos WHERE id_userPri= " + usuPri.getIdUsuario() + " and id_userSec = " + usuSec.getIdUsuario() + " and tipo != 'bloqueado'";
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            borrar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();

        }

        return borrar;
    }

    @Override
    public String misAmigosVerEstado(Usuario usuPri, Usuario usuSec) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT tipo from amigos where id_userPri = " + usuPri.getIdUsuario() + " and id_userSec =" + usuSec.getIdUsuario();

        String usuarioEstado = null;

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                usuarioEstado = rs.getString(1);

            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener mis amigos");
            e.printStackTrace();
        }

        return usuarioEstado;
    }

    @Override
    public boolean averiguarSiestaAmigos(Usuario usuPri, Usuario usuSec) {
        //SELECT * FROM `amigos` where id_userPri = 27 and id_userSec = 33
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        boolean consulta = false;
        String sql = "SELECT * FROM `amigos` where id_userPri = " + usuSec.getIdUsuario() + " and id_userSec = " + usuPri.getIdUsuario();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                consulta = true;
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método boton de amigos");
            e.printStackTrace();
        }

        return consulta;
    }

    @Override
    public void insertarBloqueado(Usuario usuPri, Usuario usuSec) {
        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO `amigos`(`id_userPri`, `id_userSec`, `tipo`) VALUES (" + usuPri.getIdUsuario() + "," + usuSec.getIdUsuario() + ",'bloqueado')";

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
    }

    @Override
    public boolean desbloquearAmigo(Usuario usuIni, Usuario usuBloqueado) {
        Connection connect = null;
        Statement stm = null;

        boolean borrar = false;

        //DELETE FROM `usuarios` WHERE `id_usuario` = 2 AND cargo ="administrador" 
        String sql = "DELETE FROM amigos WHERE id_userPri= " + usuIni.getIdUsuario() + " and id_userSec = " + usuBloqueado.getIdUsuario() + " and tipo = 'bloqueado'";
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            borrar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();

        }

        return borrar;
    }

    @Override
    public void enviarSolicitudAmistad(Usuario usu1, Usuario usu2) {
        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO `amigos`(`id_userPri`, `id_userSec`, `tipo`) VALUES (" + usu1.getIdUsuario() + "," + usu2.getIdUsuario() + ",'pendiente')";
        System.out.println(sql);
        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
    }

    @Override
    public List<Usuario> solicitudesPendientes(Usuario usuIni) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM `usuarios` where id_user IN (SELECT id_userPri FROM `amigos` where tipo = 'pendiente' AND id_userSec = " + usuIni.getIdUsuario() + ")";

        List<Usuario> listaUsuarios = new ArrayList<Usuario>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                Usuario c = new Usuario();
                c.setIdUsuario(rs.getInt(1));
                c.setNombre(rs.getString(2));
                c.setApellido(rs.getString(3));
                c.setCorreo(rs.getString(4));
                c.setEstado(rs.getString(5));
                c.setNick(rs.getString(6));
                c.setContrasenia(rs.getString(7));
                c.setFoto(rs.getString(8));

                listaUsuarios.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaUsuarios;
    }

    @Override
    public void aceptarAmigos(Usuario usuIni, Usuario usuPerfil) {
        Connection connect = null;
        Statement stm = null;
        String sql = "UPDATE `amigos` SET `tipo` = 'amigos' WHERE `amigos`.`id_userPri` = " + usuPerfil.getIdUsuario() + " AND `amigos`.`id_userSec` = " + usuIni.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);

        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
    }

    @Override
    public void insertarAmigoNuevo(Usuario usuPerfil, Usuario usuIni) {
        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO `amigos`(`id_userPri`, `id_userSec`, `tipo`) VALUES (" + usuIni.getIdUsuario() + "," + usuPerfil.getIdUsuario() + ",'amigos')";
        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
    }

    @Override
    public void actualizarEstdoAmigos(Usuario usuIni, Usuario usuPerfil) {
        Connection connect = null;
        Statement stm = null;
        String sql = "UPDATE `amigos` SET `tipo` = 'pendiente' WHERE `amigos`.`id_userPri` = " + usuPerfil.getIdUsuario() + " AND `amigos`.`id_userSec` = " + usuIni.getIdUsuario();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);

        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
    }


}
