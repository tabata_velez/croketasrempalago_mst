/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

/**
 *
 * @author Tabata
 */
public class DAOFactory {
    private static DAOFactory daoFactory;
    
    private DAOFactory(){
    }
    
    public static DAOFactory getInstance(){
        if(daoFactory == null){
            daoFactory = new DAOFactory();  
        }
        return daoFactory;
    }
    
    //usuario
    private DAOUsuario daoUsuarios;
    
    public DAOUsuario getDAOUsuarios(){
        if(daoUsuarios == null){
            daoUsuarios = new DAOUsuarioSQL(); 
        }
        return daoUsuarios;
    }
    
    
    
    //mensajes
    private DAOMensaje daoMensajes;
    
    public DAOMensaje getDAOMensajes(){
        if(daoMensajes == null){
            daoMensajes = new DAOMensajeSQL(); 
        }
        return daoMensajes;
    }
   
    
    
    //grupos
    private DAOGrupo daoGrupos;
    
    public DAOGrupo getDAOGrupos(){
        if(daoGrupos == null){
            daoGrupos = new DAOGrupoSQL(); 
        }
        return daoGrupos;
    }
    //amigos
    private DAOAmigos daoAmigos;
    
    public DAOAmigos getDAOAmigos(){
        if(daoAmigos == null){
            daoAmigos = new DAOAmigosSQL(); 
        }
        return daoAmigos;
    }
    
    //mensajes grupo
    private DAOMensajesGrupo daoMensajesGrupo;
    
    public DAOMensajesGrupo getDAOMensajesGrupo(){
        if(daoMensajesGrupo == null){
            daoMensajesGrupo = new DAOMensajeGrupoSQL();
        }
        return daoMensajesGrupo;
    }
    
}
