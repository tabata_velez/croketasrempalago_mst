/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;


import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.MensajeGrupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alumno
 */
public class DAOMensajeGrupoSQL implements DAOMensajesGrupo {

    @Override
    public List<MensajeGrupo> obtener(Grupo grupo) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM mensajes_grupo where id_grupo =" + grupo.getIdGrupo() + " order by fecha asc";

        List<MensajeGrupo> listaUsuarios = new ArrayList<MensajeGrupo>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                MensajeGrupo c = new MensajeGrupo();
                Grupo g = new Grupo();
                g.setIdGrupo(rs.getInt(2));
                Usuario u = new Usuario();
                u.setIdUsuario(rs.getInt(3));

                c.setId(rs.getInt(1));
                c.setGrupo(g);
                c.setUsuario(u);
                c.setContenido(rs.getString(4));
                c.setFecha(rs.getString(5)); 
                c.setEstado(rs.getString(6));

                listaUsuarios.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaUsuarios;
    }

    @Override
    public boolean insertarMensajeGrupo(MensajeGrupo mensajeGrupo) {

        boolean insertar = false;

        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO mensajes_grupo values (" + null + ","
                + mensajeGrupo.getGrupo().getIdGrupo() + "," + mensajeGrupo.getUsuario().getIdUsuario() + ",'" + mensajeGrupo.getContenido() + "','"
                + mensajeGrupo.getFecha() + "','" + mensajeGrupo.getEstado() + "')";

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            insertar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
        return insertar;
    }

}
