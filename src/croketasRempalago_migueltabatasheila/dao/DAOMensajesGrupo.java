/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;


import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.MensajeGrupo;
import java.util.List;

/**
 *
 * @author Alumno
 */
public interface DAOMensajesGrupo {
    public List<MensajeGrupo> obtener(Grupo grupo);
    public boolean insertarMensajeGrupo(MensajeGrupo mensajeGrupo);
}
