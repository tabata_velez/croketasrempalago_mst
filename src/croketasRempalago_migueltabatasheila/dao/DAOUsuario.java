/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import java.util.List;
import croketasRempalago_migueltabatasheila.modelo.Usuario;

/**
 *
 * @author Tabata
 */
public interface DAOUsuario {
    public boolean registrarUsuario(Usuario usuario);
    public boolean borrarUsuario(Usuario usuario);
    public List<Usuario> obtener();
    public boolean actualizarNombre(Usuario usuario, String nombreACambiar);
    public boolean actualizarApellido(Usuario usuario, String apellidoACambiar);
    public boolean actualizarCorreo(Usuario usuario, String correoACambiar);
    public boolean actualizarEstado(Usuario usuario, String estadoACambiar);
    public boolean actualizarNick(Usuario usuario, String nickACambiar);
    public boolean actualizarPass(Usuario usuario, String passACambiar);
    public boolean actualizarFoto(Usuario usuario, String fotoACambiar);
    public List<Usuario> buscarUsuarios(String buscarUsuario);
    public String passEncriptada(String contrasenya);
   
    
}
