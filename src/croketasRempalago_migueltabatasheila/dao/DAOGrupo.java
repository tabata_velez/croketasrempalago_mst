/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.util.List;

/**
 *
 * @author Tabata
 */
public interface DAOGrupo {
    public List<Grupo>obtenerGrupos(Usuario usuIni);
    public boolean registrarGrupo(Grupo grupo);
    public boolean actualizarID(Grupo grupo ,String idCambiar);
    public boolean actualizarNombre(Grupo grupo,String nombreCambiar);
    public boolean actualizartam(Grupo grupo,int tamCambiar);
    public boolean actualizarFoto(Grupo grupo,String fotoCambiar);
    public boolean actualizarTodo(Grupo grupo,String idCambiar,String nombreCambiar,String tamCambiar,String fotoCambiar);
    public boolean eliminarGrupo(Grupo grupo);
    public List<Usuario> integrantesGrupo(Grupo grupo);
    public boolean aniadirParticipante(Usuario usuario,Grupo grupo);
    public boolean salirdelGrupo(Usuario usuario, Grupo grupo);
    public List<Grupo> solicitudPendienteGrupo(Usuario usuario);
    public boolean aceptarCambiarTipo(Usuario usu1, Grupo grupo);
    public String misGruposVerEstado(Usuario usuPri, Grupo grupo);
    public boolean eliminarGrupoSolicitud(Usuario usuario,Grupo grupo);
    public List<Grupo> todoslosgrupos();
    public String verEstadoGrupo(Usuario usuario , Grupo grupo);
}
