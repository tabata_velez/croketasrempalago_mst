/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package croketasRempalago_migueltabatasheila.dao;

import croketasRempalago_migueltabatasheila.modelo.Grupo;
import croketasRempalago_migueltabatasheila.modelo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tabata
 */
public class DAOGrupoSQL implements DAOGrupo {

    @Override
    public List<Grupo> obtenerGrupos(Usuario usuarioIni) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM `grupos` WHERE id_grupo IN"
                + " (SELECT id_grupo FROM `mis_grupos` WHERE id_user = " + usuarioIni.getIdUsuario() + ")";

        List<Grupo> listaGrupo = new ArrayList<Grupo>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                Grupo g = new Grupo();
                g.setIdGrupo(rs.getInt(1));
                g.setNombre(rs.getString(2));
                g.setFoto(rs.getString(4));
                g.setTamanio(rs.getInt(3));

                listaGrupo.add(g);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaGrupo;
    }

    @Override
    public boolean registrarGrupo(Grupo grupo) {
        boolean registrar = false;

        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO grupos values ("+null+",'"
                + grupo.getNombre() + "'," + grupo.getTamanio() + ",'" + grupo.getFoto() + "')";

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            registrar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
        return registrar;
    }

    @Override
    public boolean actualizarNombre(Grupo grupo, String nombreCambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE grupos SET nombre='" + nombreCambiar + "' WHERE id_grupo=" + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean eliminarGrupo(Grupo grupo) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "DELETE FROM grupos WHERE id_grupo " + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarID(Grupo grupo, String idCambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE grupos SET id_grupo='" + idCambiar + "' WHERE id_grupo=" + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizartam(Grupo grupo, int tamCambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE grupos SET tamanio=" + tamCambiar + " WHERE id_grupo=" + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarFoto(Grupo grupo, String fotoCambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE grupos SET foto='" + fotoCambiar + "' WHERE id_grupo=" + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public boolean actualizarTodo(Grupo grupo, String idCambiar, String nombreCambiar, String tamCambiar, String fotoCambiar) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE grupos SET nombre,id_grupo,tamanio,foto='" + nombreCambiar + idCambiar + tamCambiar + fotoCambiar + "' WHERE id_grupo=" + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public List<Usuario> integrantesGrupo(Grupo grupo) {
        /**/
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM usuarios where id_user IN (SELECT id_user FROM mis_grupos WHERE id_grupo=" + grupo.getIdGrupo() + " AND tipo= 'aceptado' )";
        List<Usuario> listaGrupoIntegrantes = new ArrayList<Usuario>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                Usuario c = new Usuario();
                c.setIdUsuario(rs.getInt(1));
                c.setNombre(rs.getString(2));
                c.setApellido(rs.getString(3));
                c.setCorreo(rs.getString(4));
                c.setEstado(rs.getString(5));
                c.setNick(rs.getString(6));
                c.setContrasenia(rs.getString(7));
                c.setFoto(rs.getString(8));

                listaGrupoIntegrantes.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaGrupoIntegrantes;
    }

    @Override
    public boolean aniadirParticipante(Usuario usuario, Grupo grupo) {
        boolean insertar = false;

        Statement stm = null;
        Connection con = null;
        String sql = "INSERT INTO mis_grupos values (" + usuario.getIdUsuario() + ","
                + grupo.getIdGrupo()+ ",'pendiente')";
        System.out.println(sql);
        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            stm.execute(sql);

            insertar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método registrar");
            e.printStackTrace();
        }
        return insertar;

    }

    @Override
    public boolean salirdelGrupo(Usuario usuario, Grupo grupo) {

        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "DELETE FROM mis_grupos WHERE id_user=" + usuario.getIdUsuario() + " and id_grupo=" + grupo.getIdGrupo();
        System.out.println(sql);
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;

    }

    @Override
    public List<Grupo> solicitudPendienteGrupo(Usuario usuario) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM `grupos` where id_grupo IN (SELECT id_grupo FROM `mis_grupos` where tipo = 'pendiente' AND id_user = " + usuario.getIdUsuario() + ")";

        List<Grupo> listaUsuarios = new ArrayList<Grupo>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                Grupo g = new Grupo();

                g.setIdGrupo(rs.getInt(1));
                g.setNombre(rs.getString(2));
                g.setFoto(rs.getString(4));

                listaUsuarios.add(g);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaUsuarios;

    }

    @Override
    public boolean aceptarCambiarTipo(Usuario usu1, Grupo grupo) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "UPDATE `mis_grupos` SET `tipo` = 'aceptado' WHERE `mis_grupos`.`id_user` = " + usu1.getIdUsuario() + " AND `mis_grupos`.`id_grupo` = " + grupo.getIdGrupo();
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;
    }

    @Override
    public String misGruposVerEstado(Usuario usuPri, Grupo grupo) {
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT tipo from mis_grupos where id_user = " + usuPri.getIdUsuario() + " and id_grupo =" + grupo.getIdGrupo();

        String usuarioEstado = null;

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                usuarioEstado = rs.getString(1);

            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener mis amigos");
            e.printStackTrace();
        }

        return usuarioEstado;
    }

    @Override
    public boolean eliminarGrupoSolicitud(Usuario usuario, Grupo grupo) {
        Connection connect = null;
        Statement stm = null;

        boolean actualizar = false;

        String sql = "DELETE FROM mis_grupos WHERE id_user=" + usuario.getIdUsuario() + " and id_grupo=" + grupo.getIdGrupo();
        System.out.println(sql);
        try {
            connect = ConfiguracionBBDD.configuracionMysql();
            stm = connect.createStatement();
            stm.execute(sql);
            actualizar = true;
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método actualizar");
            e.printStackTrace();
        }
        return actualizar;

    }

    @Override
    public List<Grupo> todoslosgrupos() {
              Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM grupos";

        List<Grupo> listaGrupoIntegrantes = new ArrayList<Grupo>();

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                Grupo c = new Grupo();
                c.setIdGrupo(rs.getInt(1));
                c.setNombre(rs.getString(2));
                c.setTamanio(rs.getInt(3));
                c.setFoto(rs.getString(4));

                listaGrupoIntegrantes.add(c);
            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener");
            e.printStackTrace();
        }

        return listaGrupoIntegrantes;
    }

    @Override
    public String verEstadoGrupo(Usuario usuario, Grupo grupo) {
       Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT tipo from grupos where id_userPri = " + usuario.getIdUsuario() + " and id_userSec =" + grupo.getIdGrupo();

        String usuarioEstado = null;

        try {
            con = ConfiguracionBBDD.configuracionMysql();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                usuarioEstado = rs.getString(1);

            }

            stm.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase ClienteDaoImple, método obtener mis amigos");
            e.printStackTrace();
        }

        return usuarioEstado;
    }
    
    
}
